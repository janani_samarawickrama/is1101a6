#include <stdio.h>
#include <stdlib.h>

void triangle(int);

void main()
{
    int i, j, n;

    printf("Enter the number of lines: ");
    scanf("%d", &n);

    for(i=1; i<=n; i++)
    {

        for(j=i; j>=1; j--)
        {
            printf("%d", j);
        }

        printf("\n");
    }

   
}
