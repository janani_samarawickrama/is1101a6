#include <stdio.h>
#include <stdlib.h>

int fibonacci(int);

int main()
{
  int n, i = 0, j;

  printf("Enter the term: ");
  scanf("%d", &n);

  printf("Fibonacci series terms are:\n");

  for (j = 1; j <= n; j++)
  {
    printf("%d\n", fibonacci(i));
    i++;
  }

  return 0;
}

int fibonacci(int n)
{
  if (n == 0 || n == 1)
    return n;
  else
    return (fibonacci(n-1) + fibonacci(n-2));
}
